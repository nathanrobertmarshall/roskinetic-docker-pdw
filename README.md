# roskinetic-docker

Here are the Dockerfiles, supporting scripts, and instructions to build an arm64 Docker image based on Ubuntu Xenial that has ROS Kinetic installed and a variety of ROS packages.

For more information on running docker images on VOXL, see the documentation here: https://docs.modalai.com/docker-on-voxl/

If you don't want to built the docker image from source, you can download it prebuilt from downloads.modalai.com

## Prerequisites

*  Follow the Quick Start instructions to install the system image and software bundles: https://docs.modalai.com/voxl-quickstarts/

* Make sure your VOXL has an internet connection through wifi or other network connection https://docs.modalai.com/wifi-setup/

* Make sure docker in configured on voxl:

```bash
yocto:/# voxl-configure-docker-support.sh

Stopping original docker service
Enabling our own services docker-start & docker-prepare
starting docker-start.service
loading hello-world docker image
successfully loaded hello-world
starting docker-prepare service

done configuring voxl-docker-support
```

more details on running docker on VOXL can be found here: https://docs.modalai.com/docker-on-voxl/


## Copy files to the target

``` bash
me@mylaptop:~/roskinetic-docker$ adb shell mkdir -p /data/docker/roskinetic
me@mylaptop:~/roskinetic-docker$ adb push Dockerfile.xenial* /data/docker/roskinetic
me@mylaptop:~/roskinetic-docker$ adb push generator.sh /data/docker/roskinetic
```

## Build the Docker images on target

Log into a bash shell on VOXL through either ADB or ssh. Here we use ADB:

```bash
me@mylaptop~/$ adb shell
/ # bash
yocto:/# echo $SHELL
/bin/bash
```

Now build the docker files in order. The dockerfiles build on eachother and are split up to give you more flexibility to modify these to construct your own docker image.

Note, this is a slow process and make take an hour or two.


```bash
yocto:/# cd /data/docker/roskinetic
yocto:/data/docker/roskinetic# docker build -t roskinetic-xenial:base -f Dockerfile.xenial-base .
yocto:/data/docker/roskinetic# docker build -t roskinetic-xenial:minimal -f Dockerfile.xenial-minimal .
yocto:/data/docker/roskinetic# docker build -t roskinetic-xenial:v1.0 -f Dockerfile.xenial-packages .
```


## Run the image

First list the installed images to make sure everything built and installed correctly

```bash
yocto:/home/root# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED              VIRTUAL SIZE
roskinetic-xenial   v1.0                ef8f2f502423        About a minute ago   1.633 GB
roskinetic-xenial   minimal             3be634984ca0        2 days ago           1.153 GB
roskinetic-xenial   base                2aa1c8bdc85b        2 days ago           914.1 MB
aarch64/ubuntu      xenial              027c36187aa4        2 years ago          109.7 MB
hello-world         latest              efc161607398        3 years ago          2.088 kB

```

The final image we want to run and use is "roskinetic-xenial:v1.0". The minial and base images are snapshots of the build process for the first two docker files.

```bash
yocto:/home/root# docker run -it --rm --net=host roskinetic-xenial:v1.0
roskinetic:root$
```

Or better yet, if you are using voxl-docker-support >= v1.1 as included with voxl-software-bundle 0.0.3 you can launch the docker image with voxl-docker which is faster to type and will mount your Yocto home directory inside the docker image at ~/yoctohome/

```bash
yocto:/home/root# voxl-docker -i roskinetic-xenial:v1.0
roskinetic:~$ ls
ros_catkin_ws  yoctohome
roskinetic:~$
```

## optional: give the image your own tag

To save time, you can tag the docker image you just built with a shorter name.

```bash
yocto:/home/root# docker tag roskinetic-xenial:v1.0 kinetic
yocto:/home/root# voxl-docker -i kinetic
roskinetic:~$ ls
ros_catkin_ws  yoctohome
roskinetic:~$
```

## Archive the image

This will make a transferable archive of the docker image you just built. The output of this will match the prebuilt image we distribute at downloads.modalai.com

```bash
yocto:/# docker save roskinetic-xenial:v1.0 | gzip > /data/roskinetic-xenial_v1_0.tgz
```

## PDW fork notes
Build focal-compile image on your dev environment.
```
docker build -t focal-pdw:compile -f Dockerfile.focal-compile .
```
Run focal-compile.
```
docker run -it --rm --net=host focal-pdw:compile
```
Use the environment variable "CC" as gcc to build c code with the right architecture. 

